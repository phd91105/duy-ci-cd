const express = require('express')
const app = express()
const port = process.env.PORT || 5000

app.get('/', (req, res) => {
  res.send({ msg: 'Hello World!' })
})
app.get("/sum", (req, res)=>{
  let numA = parseInt(req.query.numA)
  let numB = parseInt(req.query.numB)
  res.send({ sum: `${ numA + numB }` })
})
app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`)
})
